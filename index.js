// USING DOM:
// Retrieve an element from webpage
const txtFirstName =document.querySelector('#txt-first-name')
const spanFullName = document.querySelector('#span-full-name' );

/*
ALTERNATIVE IN RETRIEVING AN ELEMENT: getElement
for id
document.getElementById('txt-first-name');
for class
document.getElementsByClassName('tclass-name');
for tags or element type:
document.getElementsByTagName('input');

*/
// EVENT LISTENER:https://www.w3schools.com/jsref/dom_obj_event.asp
//  an interaction between the user and webpage.
// keyup-is a reserved key word.
txtFirstName.addEventListener('keyup',(event)=>{
	spanFullName.innerHTML =txtFirstName.value;
})
txtLastName.addEventListener('keyup',(event)=>{
	spanFullName.innerHTML =txtLastName.value;
})

// assign same event to multiple listener:
txtFirstName.addEventListener('keyup', (event)=>{
	// event.target -will the ELEMENT where the event happened
	console.log(event.target);
	// .value- gets the value of the input object
	console.log(event.target.value)
})